$(document).ready(function () {
    document.getElementById('startProgress').addEventListener('click',startProgress,false);
});

function startProgress() {
    var urlList  = $("#url-list")[0].value;
    var listURL = _.split(urlList, "\n");

    var promises = listURL.map((url) => extractQuest(url));

    var pro = _.chunk(promises, 1);

    Promise.all(promises).then((results) => {
        console.log(JSON.stringify(results));
        var data = {
            question: JSON.stringify(results),
            package: `${$("#package-name")[0].value}`
        };

        var http = new XMLHttpRequest();
        var url = "https://ii0esl3ea7.execute-api.us-east-1.amazonaws.com/staging";
        http.open("POST", url, true);
        http.setRequestHeader("Content-type", "application/json");
        http.send(JSON.stringify(data)); 
    });
}


function extractQuest(url){
    return new Promise(function(resolve, reject) {
        $.get(url).done((response) => {
            var el = $( '<div></div>' );
            el.html(response);
            var content = $(".entry-content", el).children();
            var question = content[0].innerHTML;
            var ansList = [content[4],content[5], content[6], content[7]];
            var ans =  _.map(ansList, extractAns);
            var trueAnsIndex = _.findIndex(ansList, function(o) { return o.className == 'rightAnswer'; });

            var blockquote = $('blockquote', el).children();

            var imageUrl = '';

            if (blockquote[0]){
                var imageEl = $( '<div></div>' );
                imageEl.html(blockquote[0].innerHTML);
                if ($('a', imageEl).children()[0]){
                    imageUrl = $('a', imageEl).children()[0].src;
                }
            }
            var amzQuote = '';
            if (blockquote[1]){
                var amzQuote = blockquote[1].innerHTML;
            }

            // var amzQuote = blockquote[1];

            quest = {
                question: question,
                choices: ans,
                trueAns: trueAnsIndex,
                quote: {
                    imageRef: imageUrl,
                    amzRef: amzQuote
                }
            }
            console.log(quest);
            resolve(quest);
        });
    });
}

function type(obj){
    return Object.prototype.toString.call(obj).slice(8, -1);
}

function extractAns(n) {
  return strip(n.innerHTML.replace('<br>', '').replace('\n', ''));
}

function strip(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}